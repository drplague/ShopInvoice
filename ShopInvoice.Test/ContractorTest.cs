﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopInvoiceClass;
using System;

namespace ShopInvoice.Test
{
    [TestClass]
    public class ContractorTest
    {
        [TestMethod]
        public void CanBeAdded_Validate_RetursTrue()
        {
            // Arrange

            // Act
            bool StatusResult = (DBContractor.getContractorsRange() == System.Data.ConnectionState.Open);
            //Assert
            Assert.IsFalse(StatusResult);
        }
    }
}
