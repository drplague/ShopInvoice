﻿namespace ShopInvoiceClass
{
    public class PKWiU
    {
        private int id;
        private string badge;
        private string name;
        public int ID
        {
            get => this.id;
            set => this.id = value;
        }
        public string Badge
        {
            get => this.badge;
            set => this.badge = value;
        }
        public string Name
        {
            get => this.name;
            set => this.name = value;
        }
        public PKWiU(int id, string Badge, string Name)
        {
            this.id = id;
            this.badge = Badge;
            this.name = Name;
        }
        public PKWiU()
        {

        }
    }
}
