﻿namespace ShopInvoiceClass
{
    public class Product
    {
        private int id;
        private string name;
        private decimal price = 0.00m;
        private short ptu;
        private int pkwiu;
        private int quantity;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public decimal Price { get => price; set => price = value; }
        public short PTU { get => ptu; set => ptu = value; }
        public int PKWiU { get => pkwiu; set => pkwiu = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public Product(int id, string name, decimal price, short ptu, int pkwiu, int quantity)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.ptu = ptu;
            this.pkwiu = pkwiu;
            this.quantity = quantity;
        }
        public Product(int id,short ptu ,int quantity)
        {
            this.id = id;
            this.ptu = ptu;
            this.quantity = quantity;
        }
    }
}
