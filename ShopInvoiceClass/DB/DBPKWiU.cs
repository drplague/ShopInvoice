﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ShopInvoiceClass
{
    public static class DBPKWiU
    {
        private static PKWiU pkwiu;
        public static PKWiU GetPKWiUByID(int id)
        {
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.Select("PKWiU",id))
                {
                    reader.Read();
                    pkwiu = new PKWiU(Convert.ToInt32(reader["Id"]), reader["Badge"].ToString(), reader["Name"].ToString());
                }
            }
            return pkwiu;
        }
        public static List<PKWiU> GetByName(string name)
        {
            List<PKWiU> pkwiuList = new();
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("SELECT * FROM [PKWiU] WHERE [Name] Like(@name)", con);
                cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        pkwiuList.Add(new PKWiU(Convert.ToInt32(reader["Id"]), reader["Badge"].ToString(), reader["Name"].ToString()));
                }
            }
            return pkwiuList;
        }
        public static List<PKWiU> GetByBadge(string badge)
        {
            List<PKWiU> pkwiuList = new();
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("SELECT * FROM [PKWiU] WHERE [Badge] Like(@badge)", con);
                cmd.Parameters.AddWithValue("@badge", "%" + badge + "%");
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        pkwiuList.Add(new PKWiU(Convert.ToInt32(reader["Id"]), reader["Badge"].ToString(), reader["Name"].ToString()));
                }
            }
            return pkwiuList;
        }
        public static int Count
        {
            get
            {
                int count = 0;
                using (DBConnection db = new())
                {
                    db.GetConnection.Open();
                    count = db.Count("PKWiU");
                }
                return count;
            }
        }
        public static List<PKWiU> GetByNameAndBadge(string name, string badge)
        {
            List<PKWiU> pkwiuList = new();
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("SELECT * FROM [PKWiU] WHERE [Name] Like(@name) AND [Badge] Like(@badge)", con);
                cmd.Parameters.AddWithValue("@badge", "%" + badge + "%");
                cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        pkwiuList.Add(new PKWiU(Convert.ToInt32(reader["Id"]), reader["Badge"].ToString(), reader["Name"].ToString()));
                }
            }
            return pkwiuList;
        }
        public static int InsertPKWiU(PKWiU p)
        {
            int affectedRows;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("INSERT INTO [PKWiU] ([Id],[Badge],[Name]) VALUES (@id,@badge,@name)", con);
                cmd.Parameters.AddWithValue("@id", p.ID);
                cmd.Parameters.AddWithValue("@badge", p.Badge);
                cmd.Parameters.AddWithValue("@name", p.Name);
                affectedRows = cmd.ExecuteNonQuery();
            }
            return affectedRows;
        }
        //public static int ReadFromXML()
        //{
        //    int affectedRows = 0;
        //    List<PKWiU> pkwiuList = new List<PKWiU>();
        //    XmlSerializer deserialize = new XmlSerializer(pkwiuList.GetType());
        //    if (File.Exists(@"C://Users/minec/Downloads/pkwiu.xml"))
        //    {
        //        using (TextReader filestream = new StreamReader(@"C://Users/minec/Downloads/pkwiu.xml"))
        //        {
        //            pkwiuList = (List<PKWiU>)deserialize.Deserialize(filestream);
        //        }
        //        foreach (PKWiU p in pkwiuList)
        //        {
        //            affectedRows += InsertPKWiU(p);
        //        }
        //    }
        //    return affectedRows;
        //}
    }
}
