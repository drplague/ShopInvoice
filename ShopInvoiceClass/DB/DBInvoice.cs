﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ShopInvoiceClass
{
    public static class DBInvoice
    {
        public static Invoice GetByID(int id)
        {
            Invoice invoice = null;

            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("SELECT * FROM [Invoices] WHERE [Id] = @id", con);
                cmd.Parameters.AddWithValue("@id", id);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                        invoice = new((int)reader["Id"], reader["Name"].ToString(), (int)reader["ContractorID"], (decimal)reader["Charge"]);
                }
                cmd = new("SELECT * FROM [InvoiceProducts] WHERE [InvoiceId] = @invoiceID", con);
                cmd.Parameters.AddWithValue("@invoiceID", invoice.Id);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                            invoice.InvoiceProducts.Add(new((int)reader["ProductID"], (short)DBProducts.GetProduct((int)reader["ProductID"]).PTU, (int)reader["Quantity"]));
                    }
                }
            }
            return invoice;
        }
        public static int Insert(Invoice invo)
        {
            int affectedRows = 0;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("INSERT INTO [Invoices] ([Id],[ContractorID],[Charge],[Name]) VALUES (@id,@contractorid,@charge,@name)", con);
                cmd.Parameters.AddWithValue("@id", invo.Id);
                cmd.Parameters.AddWithValue("@contractorid", invo.ContractorID);
                cmd.Parameters.AddWithValue("@charge", invo.Charge);
                cmd.Parameters.AddWithValue("@name", invo.Name);
                affectedRows = cmd.ExecuteNonQuery();
                foreach (Product p in invo.InvoiceProducts)
                {
                    cmd = new("INSERT INTO [InvoiceProducts] ([Id],[InvoiceID],[ProductID],[Quantity]) VALUES (@id,@invoiceid,@productid,@quantity)", con);
                    cmd.Parameters.AddWithValue("@id", LastProduct() + 1);
                    cmd.Parameters.AddWithValue("@invoiceid", invo.Id);
                    cmd.Parameters.AddWithValue("@productid", p.Id);
                    cmd.Parameters.AddWithValue("@quantity", p.Quantity);
                    cmd.ExecuteNonQuery();
                }
            }
            return affectedRows;
        }
        public static int Edit(Invoice invo)
        {
            int affectedRows = 0;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("UPDATE [Invoice] SET [Id] = @id,[ContractorID] = @contractorid,[Charge] = @charge,[Name] = @name)", con);
                cmd.Parameters.AddWithValue("@id", invo.Id);
                cmd.Parameters.AddWithValue("@contractorid", invo.ContractorID);
                cmd.Parameters.AddWithValue("@charge", invo.Charge);
                cmd.Parameters.AddWithValue("@name", invo.Name);
                affectedRows = cmd.ExecuteNonQuery();
            }
            return affectedRows;
        }
        public static int LastProduct()
        {
            int count = 0;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                count = db.LastID("InvoiceProducts");
            }
            return count;
        }
        public static int Count(string name)
        {
            int count = 0;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("SELECT COUNT([Id]) AS 'Count' FROM [Invoices] WHERE [Name] Like(@name)", con);
                cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    count = (int)reader["Count"];
                }
            }
            return count;
        }
        public static int Count()
        {
            int count = 0;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                count = db.Count("Invoices");
            }
            return count;
        }
        public static List<Invoice> GetList()
        {
            List<Invoice> invoList = new();
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.Select("Invoices"))
                {
                    while (reader.Read())
                        if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                            invoList.Add(new((int)reader["Id"], reader["Name"].ToString(), (int)reader["ContractorID"], (decimal)reader["Charge"]));
                }
            }
            return invoList;
        }
        public static List<Invoice> GetList(int id)
        {
            List<Invoice> invoiceList = new();
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("SELECT * FROM [Invoices] WHERE [ContractorID] = @id", con);
                cmd.Parameters.AddWithValue("@id", id);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                            invoiceList.Add(new((int)reader["Id"], reader["Name"].ToString(), (int)reader["ContractorID"], (decimal)reader["Charge"]));
                }
            }
            return invoiceList;
        }
        public static int LastId()
        {
            int tmp = 0;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                tmp = db.LastID("Invoices");
            }
            return tmp;
        }
        public static int Remove(int id)
        {
            int tmp = 0;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                db.Delete("Invoices", id);
                SqlCommand cmd = new("DELETE FROM [InvoiceProducts] WHERE [InvoiceId] = @id", db.GetConnection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
            return tmp;
        }
    }
}
