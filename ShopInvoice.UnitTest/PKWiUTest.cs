﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopInvoiceClass;

namespace ShopInvoice.UnitTest
{
    [TestClass]
    class PKWiUTest
    {
        [TestMethod]
        public void ReadFromFile_ReadAllPkwiuFromFile_ReturnTrue()
        {
            //arrange
            //act
            var result = DBPKWiU.ReadFromXML();
            //assert
            Assert.IsTrue(result == 8779);
        }
    }
}
