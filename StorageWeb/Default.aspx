﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="StorageWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Stan Magazynowy<asp:GridView ID="ProductsGrid" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AutoGenerateColumns="False" Width="341px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:DynamicField HeaderText="Nazwa" />
                <asp:DynamicField DataField="gridProdPrice" HeaderText="Cena" />
                <asp:DynamicField DataField="formDataPTU" HeaderText="PTU" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </h1>
        <div id="controls">
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="&lt;&lt;" />
            <asp:Label ID="pageID" runat="server" Text="Label"></asp:Label>
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="&gt;&gt;" />
        </div>
    </div>

</asp:Content>
