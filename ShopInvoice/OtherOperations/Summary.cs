﻿using ShopInvoiceClass;
using ShopInvoiceClass.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopInvoiceWidows.OtherOperations
{
    public partial class Summary : Form
    {
        public Summary()
        {
            InitializeComponent();
            refreshTable();
        }
        private int page = 1;
        private void prevButton_Click(object sender, EventArgs e)
        {
            page--;
            if (page < 1)
                page = Convert.ToInt32(Math.Ceiling(DBOtherOperation.Count / 10.0));
            pageID.Text = page.ToString();
            refreshTable();
        }
        private void refreshTable()
        {
            summaryGrid.Rows.Clear();
            foreach (OtherOperation c in DBOtherOperation.Get(page))
            {
                int tmp = summaryGrid.Rows.Add(c.Id, c.Date, DBProducts.GetProduct(c.Productid).Name, 0, c.Quantity);
                if (c.Type == 1)
                    summaryGrid.Rows[tmp].Cells[3].Value = "Dostawa";
                else if (c.Type == 2)
                    summaryGrid.Rows[tmp].Cells[3].Value = "Op. Poza Fakturowe";
                else
                    summaryGrid.Rows[tmp].Cells[3].Value = "Inne";
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            page++;
            if (page > Convert.ToInt32(Math.Ceiling(DBOtherOperation.Count / 10.0)))
                page = 1;
            pageID.Text = page.ToString();
            refreshTable();
        }
    }
}
