﻿
namespace ShopInvoiceWidows.OtherOperations
{
    partial class Summary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.summaryGrid = new System.Windows.Forms.DataGridView();
            this.formID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formOperationType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nextButton = new System.Windows.Forms.Button();
            this.prevButton = new System.Windows.Forms.Button();
            this.pageID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.summaryGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // summaryGrid
            // 
            this.summaryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.summaryGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.formID,
            this.formDate,
            this.formProductName,
            this.formOperationType,
            this.formQuantity});
            this.summaryGrid.Location = new System.Drawing.Point(12, 12);
            this.summaryGrid.MultiSelect = false;
            this.summaryGrid.Name = "summaryGrid";
            this.summaryGrid.ReadOnly = true;
            this.summaryGrid.RowTemplate.Height = 25;
            this.summaryGrid.Size = new System.Drawing.Size(776, 405);
            this.summaryGrid.TabIndex = 0;
            // 
            // formID
            // 
            this.formID.HeaderText = "ID";
            this.formID.Name = "formID";
            this.formID.ReadOnly = true;
            // 
            // formDate
            // 
            this.formDate.HeaderText = "Data";
            this.formDate.Name = "formDate";
            this.formDate.ReadOnly = true;
            // 
            // formProductName
            // 
            this.formProductName.HeaderText = "Nazwa Produktu";
            this.formProductName.Name = "formProductName";
            this.formProductName.ReadOnly = true;
            // 
            // formOperationType
            // 
            this.formOperationType.HeaderText = "Operacja";
            this.formOperationType.Name = "formOperationType";
            this.formOperationType.ReadOnly = true;
            // 
            // formQuantity
            // 
            this.formQuantity.HeaderText = "Ilosc";
            this.formQuantity.Name = "formQuantity";
            this.formQuantity.ReadOnly = true;
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(425, 423);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = ">>";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(301, 423);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 23);
            this.prevButton.TabIndex = 6;
            this.prevButton.Text = "<<";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // pageID
            // 
            this.pageID.AutoSize = true;
            this.pageID.Location = new System.Drawing.Point(394, 427);
            this.pageID.Name = "pageID";
            this.pageID.Size = new System.Drawing.Size(13, 15);
            this.pageID.TabIndex = 5;
            this.pageID.Text = "1";
            // 
            // Summary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.pageID);
            this.Controls.Add(this.summaryGrid);
            this.Name = "Summary";
            this.Text = "Podsumowanie";
            ((System.ComponentModel.ISupportInitialize)(this.summaryGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView summaryGrid;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Label pageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn formID;
        private System.Windows.Forms.DataGridViewTextBoxColumn formDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn formProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn formOperationType;
        private System.Windows.Forms.DataGridViewTextBoxColumn formQuantity;
    }
}