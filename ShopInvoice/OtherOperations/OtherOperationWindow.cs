﻿using ShopInvoiceClass;
using ShopInvoiceClass.DB;
using ShopInvoiceWidows.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopInvoiceWidows.OtherOperations
{
    public partial class OtherOperationWindow : Form
    {
        private int operationType = 0;

        public OtherOperationWindow(int i)
        {
            InitializeComponent();
            operationType = i;
        }
        private List<OtherOperation> otherList= new();
        private void productToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductsWindow pw = new();
            pw.ShowDialog();
            int tmp = pw.GetID;
            otherList.Add(new(operationType, tmp, 0));
            operationsGrid.Rows.Add(DBProducts.GetProduct(tmp).Name);
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 1)
            {
                otherList[e.RowIndex].Quantity = Convert.ToInt32(operationsGrid.Rows[e.RowIndex].Cells[1].Value);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            DBOtherOperation.Insert(otherList);
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
