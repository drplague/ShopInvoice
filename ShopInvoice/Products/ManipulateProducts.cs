﻿using ShopInvoiceClass;
using System.Windows.Forms;

namespace ShopInvoiceWidows.Products
{
    public partial class ManipulateProducts : Form
    {
        int id = -1;
        int pkwiu;
        public ManipulateProducts()
        {
            InitializeComponent();
        }
        public ManipulateProducts(Product p)
        {
            InitializeComponent();
            this.id = p.Id;
            formName.Text = p.Name;
            formPrice.Value = p.Price;
            formPTU.Value = p.PTU;
            pkwiu = p.PKWiU;
            formQuantity.Value = p.Quantity;

        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            SelectPkwiu sp = new();
            sp.ID = pkwiu;
            sp.ShowDialog();
            pkwiu = sp.ID;
        }

        private void saveButton_Click(object sender, System.EventArgs e)
        {
            if (id < 0)
            {
                DBProducts.Insert(new Product(DBProducts.LastId() + 1, formName.Text, formPrice.Value, (short)formPTU.Value, pkwiu, (int)formQuantity.Value));
            }
            else
            {
                DBProducts.Edit(new Product(id, formName.Text, formPrice.Value, (short)formPTU.Value, pkwiu, (int)formQuantity.Value));
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
