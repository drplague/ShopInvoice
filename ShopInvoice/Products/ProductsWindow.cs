﻿using ShopInvoiceClass;
using System;
using System.Windows.Forms;

namespace ShopInvoiceWidows.Products
{
    public partial class ProductsWindow : Form
    {
        private int page = 1;
        private int selectedid = 0;
        public int GetID
        {
            get => this.selectedid;
        }
        public ProductsWindow()
        {
            InitializeComponent();
            refreshTable();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            page++;
            if (page > Convert.ToInt32(Math.Ceiling(DBProducts.Count / 10.0)))
                page = 1;
            pageID.Text = page.ToString();
            refreshTable();
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            page--;
            if (page < 1)
                page = Convert.ToInt32(Math.Ceiling(DBProducts.Count / 10.0));
            pageID.Text = page.ToString();
            refreshTable();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            ManipulateProducts mp = new(DBProducts.GetProducts()[productsGrid.SelectedRows[0].Index]);
            mp.ShowDialog();
            refreshTable();
        }
        private void refreshTable()
        {
            productsGrid.Rows.Clear();
            foreach (Product p in DBProducts.GetProducts(page))
            {
                productsGrid.Rows.Add(p.Id, p.Name, p.Price, p.PTU, p.PKWiU, p.Quantity);
            }
        }

        private void AddButoon_Click(object sender, EventArgs e)
        {
            ManipulateProducts mp = new();
            mp.ShowDialog();
            refreshTable();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            DBProducts.Delete(Convert.ToInt32(productsGrid.SelectedRows[0].Cells[0].Value));
            refreshTable();
        }

        private void productsGrid_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            selectedid = (int)productsGrid.Rows[e.RowIndex].Cells[0].Value;
            this.Close();
        }
    }
}
