﻿
namespace ShopInvoiceWidows.Products
{
    partial class ManipulateProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.formName = new System.Windows.Forms.TextBox();
            this.formPrice = new System.Windows.Forms.NumericUpDown();
            this.formPTU = new System.Windows.Forms.NumericUpDown();
            this.formPkwiu = new System.Windows.Forms.Button();
            this.formQuantity = new System.Windows.Forms.NumericUpDown();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.formPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formPTU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nazwa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cena";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "PTU";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "PKWiU";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(-1751, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "PKWiU";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ilosc";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(389, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Product ID";
            // 
            // formName
            // 
            this.formName.Location = new System.Drawing.Point(279, 33);
            this.formName.Name = "formName";
            this.formName.Size = new System.Drawing.Size(173, 23);
            this.formName.TabIndex = 8;
            // 
            // formPrice
            // 
            this.formPrice.DecimalPlaces = 2;
            this.formPrice.Location = new System.Drawing.Point(279, 61);
            this.formPrice.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.formPrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.formPrice.Name = "formPrice";
            this.formPrice.Size = new System.Drawing.Size(173, 23);
            this.formPrice.TabIndex = 9;
            this.formPrice.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // formPTU
            // 
            this.formPTU.Location = new System.Drawing.Point(279, 89);
            this.formPTU.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.formPTU.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.formPTU.Name = "formPTU";
            this.formPTU.Size = new System.Drawing.Size(173, 23);
            this.formPTU.TabIndex = 10;
            this.formPTU.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // formPkwiu
            // 
            this.formPkwiu.Location = new System.Drawing.Point(279, 117);
            this.formPkwiu.Name = "formPkwiu";
            this.formPkwiu.Size = new System.Drawing.Size(173, 23);
            this.formPkwiu.TabIndex = 11;
            this.formPkwiu.Text = "PKWiU";
            this.formPkwiu.UseVisualStyleBackColor = true;
            this.formPkwiu.Click += new System.EventHandler(this.button1_Click);
            // 
            // formQuantity
            // 
            this.formQuantity.Location = new System.Drawing.Point(279, 145);
            this.formQuantity.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.formQuantity.Name = "formQuantity";
            this.formQuantity.Size = new System.Drawing.Size(173, 23);
            this.formQuantity.TabIndex = 12;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(12, 174);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(440, 108);
            this.saveButton.TabIndex = 13;
            this.saveButton.Text = "Zapisz";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(12, 288);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(440, 108);
            this.cancelButton.TabIndex = 14;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // ManipulateProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 405);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.formQuantity);
            this.Controls.Add(this.formPkwiu);
            this.Controls.Add(this.formPTU);
            this.Controls.Add(this.formPrice);
            this.Controls.Add(this.formName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ManipulateProducts";
            this.Text = "Produkt";
            ((System.ComponentModel.ISupportInitialize)(this.formPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formPTU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formQuantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox formName;
        private System.Windows.Forms.NumericUpDown formPrice;
        private System.Windows.Forms.NumericUpDown formPTU;
        private System.Windows.Forms.Button formPkwiu;
        private System.Windows.Forms.NumericUpDown formQuantity;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
    }
}