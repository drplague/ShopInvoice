﻿using ShopInvoiceClass;
using ShopInvoiceWidows.Contractors;
using System;
using System.Windows.Forms;

namespace ShopInvoiceWidows
{
    public partial class ContractorWindow : Form
    {
        private int page = 1;
        private int selectedID = -1;
        public int GetID
        {
            get => this.selectedID;
        }
        public ContractorWindow()
        {
            InitializeComponent();
            refreshTable();
        }
        private void refreshTable()
        {
            contractotsGrid.Rows.Clear();
            foreach (Contractor c in DBContractor.GetContractors(page))
            {
                contractotsGrid.Rows.Add(c.ID, c.Name, c.Address, c.NIP, c.REGON);
            }
        }
        private void prevButton_Click(object sender, EventArgs e)
        {
            page--;
            if (page < 1)
                page = Convert.ToInt32(Math.Ceiling(DBContractor.Count / 10.0));
            pageID.Text = page.ToString();
            refreshTable();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            page++;
            if (page > Convert.ToInt32(Math.Ceiling(DBContractor.Count / 10.0)))
                page = 1;
            pageID.Text = page.ToString();
            refreshTable();
        }

        private void contractotsGrid_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            selectedID = (int)contractotsGrid.Rows[e.RowIndex].Cells[0].Value;
            this.Close();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            ManipulateContractor mp = new(DBContractor.GetByID((int)contractotsGrid.SelectedRows[0].Cells[0].Value));
            this.Hide();
            mp.ShowDialog();
            this.Show();
            refreshTable();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            int tmp = Convert.ToInt32(contractotsGrid.SelectedRows[0].Cells[0].Value);
            DBContractor.Delete(tmp);
            refreshTable();
        }

        private void AddButoon_Click(object sender, EventArgs e)
        {
            ManipulateContractor mp = new();
            this.Hide();
            mp.ShowDialog();
            this.Show();
            refreshTable();
        }

        private void contractotsGrid_SelectionChanged(object sender, EventArgs e)
        {
            if(contractotsGrid.SelectedRows.Count > 0)
            selectedID = Convert.ToInt32(contractotsGrid.SelectedRows[0].Cells[0].Value);
        }
    }
}
