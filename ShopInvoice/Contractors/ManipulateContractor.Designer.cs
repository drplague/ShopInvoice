﻿
namespace ShopInvoiceWidows.Contractors
{
    partial class ManipulateContractor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.formContractorName = new System.Windows.Forms.TextBox();
            this.formContractorAddress = new System.Windows.Forms.TextBox();
            this.formContractorNIP = new System.Windows.Forms.TextBox();
            this.formContractorREGON = new System.Windows.Forms.TextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.CloseWindow = new System.Windows.Forms.Button();
            this.formContractorID = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Kontrahenta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nazwa Kontrahenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adres Kontrahenta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "NIP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "REGON";
            // 
            // formContractorName
            // 
            this.formContractorName.Location = new System.Drawing.Point(271, 47);
            this.formContractorName.Name = "formContractorName";
            this.formContractorName.Size = new System.Drawing.Size(187, 23);
            this.formContractorName.TabIndex = 6;
            // 
            // formContractorAddress
            // 
            this.formContractorAddress.Location = new System.Drawing.Point(271, 88);
            this.formContractorAddress.Name = "formContractorAddress";
            this.formContractorAddress.Size = new System.Drawing.Size(187, 23);
            this.formContractorAddress.TabIndex = 7;
            // 
            // formContractorNIP
            // 
            this.formContractorNIP.Location = new System.Drawing.Point(271, 129);
            this.formContractorNIP.Name = "formContractorNIP";
            this.formContractorNIP.Size = new System.Drawing.Size(187, 23);
            this.formContractorNIP.TabIndex = 8;
            // 
            // formContractorREGON
            // 
            this.formContractorREGON.Location = new System.Drawing.Point(271, 170);
            this.formContractorREGON.Name = "formContractorREGON";
            this.formContractorREGON.Size = new System.Drawing.Size(187, 23);
            this.formContractorREGON.TabIndex = 9;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(6, 205);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(452, 96);
            this.SaveButton.TabIndex = 10;
            this.SaveButton.Text = "Zapisz";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // CloseWindow
            // 
            this.CloseWindow.Location = new System.Drawing.Point(6, 307);
            this.CloseWindow.Name = "CloseWindow";
            this.CloseWindow.Size = new System.Drawing.Size(452, 96);
            this.CloseWindow.TabIndex = 11;
            this.CloseWindow.Text = "Anuluj";
            this.CloseWindow.UseVisualStyleBackColor = true;
            this.CloseWindow.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // formContractorID
            // 
            this.formContractorID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.formContractorID.AutoSize = true;
            this.formContractorID.Location = new System.Drawing.Point(414, 9);
            this.formContractorID.MaximumSize = new System.Drawing.Size(100, 15);
            this.formContractorID.MinimumSize = new System.Drawing.Size(26, 15);
            this.formContractorID.Name = "formContractorID";
            this.formContractorID.Size = new System.Drawing.Size(38, 15);
            this.formContractorID.TabIndex = 12;
            this.formContractorID.Text = "label6";
            this.formContractorID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ManipulateContractor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 409);
            this.Controls.Add(this.formContractorID);
            this.Controls.Add(this.CloseWindow);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.formContractorREGON);
            this.Controls.Add(this.formContractorNIP);
            this.Controls.Add(this.formContractorAddress);
            this.Controls.Add(this.formContractorName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ManipulateContractor";
            this.Text = "Kontrahent";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox formContractorName;
        private System.Windows.Forms.TextBox formContractorAddress;
        private System.Windows.Forms.TextBox formContractorNIP;
        private System.Windows.Forms.TextBox formContractorREGON;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button CloseWindow;
        private System.Windows.Forms.Label formContractorID;
    }
}