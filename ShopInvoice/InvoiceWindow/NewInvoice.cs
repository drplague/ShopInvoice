﻿using ShopInvoiceClass;
using ShopInvoiceWidows.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopInvoiceWidows.InvoiceWindow
{
    public partial class NewInvoice : Form
    {
        public NewInvoice()
        {
            InitializeComponent();
        }
        private Invoice invoice = new(DBInvoice.LastId() + 1, (DBInvoice.Count(DateTime.Now.ToString("yyy/MM/dd/")) + 1),0,0);
        private void kontrachentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ContractorWindow cw = new();
            cw.ShowDialog();
            invoice.ContractorID = cw.GetID;
            Contractor tmp = DBContractor.GetByID(cw.GetID);
            ContractorName.Text = tmp.Name;
            ContractorNIP.Text = tmp.NIP;
            ContractorRegon.Text = tmp.REGON;
        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductsWindow pw = new();
            pw.ShowDialog();
            Product tmp = DBProducts.GetProduct(pw.GetID);
            prodGrid.Rows.Add(tmp.Id,tmp.Name);
            invoice.InvoiceProducts.Add(new(tmp.Id,tmp.PTU,0));
        }

        private void prodGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 2)
            {
                invoice.InvoiceProducts[e.RowIndex].Quantity = Convert.ToInt32(prodGrid.Rows[e.RowIndex].Cells[2].Value);
                prodGrid.Rows[e.RowIndex].Cells[3].Value = invoice.InvoiceProducts[e.RowIndex].Quantity * DBProducts.GetProduct((int)prodGrid.Rows[e.RowIndex].Cells[0].Value).Price;
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < prodGrid.Rows.Count; i++)
                invoice.Charge += Convert.ToInt32(prodGrid.Rows[i].Cells[3].Value);
            DBInvoice.Insert(invoice);
            this.Close();
        }

        private void usunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                invoice.InvoiceProducts.RemoveAt(prodGrid.SelectedRows[0].Index);
                prodGrid.Rows.RemoveAt(prodGrid.SelectedRows[0].Index);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}
